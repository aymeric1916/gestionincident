<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{

    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);

        // Retour
        return $bug;
    }

    

    public function findAll()
    {
        $dbh = static::connectDb();

        $sth = $dbh->prepare('SELECT * FROM bug ORDER BY id');
        $sth->execute();

        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){
            $bug = new Bug();
            $bug->setId($result["id"]);
            $bug->setTitle($result["title"]);
            $bug->setDescription($result["description"]);
            $bug->setCreatedAt($result["createdAt"]);
            $bug->setClosedAt($result["closed"]);
            array_push($bugs,$bug);
        }
        return $bugs;
    }

    public function add(Bug $bug){
        $dbh = static::connectDb();

        $sql ='INSERT INTO bug (title, description, createdAt) VALUES (:title, :description, :createdAt)';
        $sth = $dbh->prepare($sql);

        $sth->execute(array(
            ":title" => $bug->getTitle(),
            ":description" => $bug->getDescription(),
            ":createdAt" => $bug->getCreatedAt()->format('Y-m-d H:i:'),
        ));
    }

    public function update(Bug $bug){

       // Modification d'un incident en BDD

    }







}
