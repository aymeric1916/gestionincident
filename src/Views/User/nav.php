<nav class="blue-grey" role="navigation">
  <div class="nav-wrapper container"><a id="logo-container" href="../src/Views/Bug/list.php" class="brand-logo">Menu</a>
    <ul class="right hide-on-med-and-down">

      <?php
      if(isset($_SESSION['user'])){
        $user = unserialize($_SESSION['user']);
      ?>
      <li><i class="material-icons">person</i></li>
      <li><?= $user->getNom(); ?></li>
      <li><a href="logout">logout</a></li>
      <?php  }  ?>

    </ul>

    <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>
