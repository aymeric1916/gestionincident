<?php

$bugs = $parameters['bugs'];

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Starter Template - Materialize</title>

   <!-- Compiled and minified CSS -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
   <!-- Compiled and minified JavaScript -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
   <!-- CSS  -->
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <link href="/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
   <link href="style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Logo</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><?php include("../Views/User/nav.php"); ?></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <div class="right">
        <img src="./images/utilisateurs.png" alt="Logo utilisateurs">
      </div>
      <h1 class="header">Liste des incidents</h1>
      <br><br>

    </div>
  </div>


  <div class="container">
    <div class="section">
      <div class="good_pos">
        <a href="../Bug/add.php" class="btn-floating btn-large waves-effect waves-light blue" ><i class="material-icons">add</i></a>
      </div>
    <br/><br/><br/>
      <table>
          <thead>
            <tr>
                <th>Id</th>
                <th>Sujet</th>
                <th>Date</th>
                <th>Cloture</th>
                <th>     </th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>4</td>
              <td>Bug d'affichage</td>
              <td>10/09/2020</td>
              <td>12/12/2019</td>
              <td><a href="#">Afficher</a></td>
            </tr>
            <?php foreach($bugs as $bug) { ?>
              <tr>
                <td><?= $bug->getId();?></td>
                <td><?= $bug->getTitle();?></td>
                <td><?php echo $bug->getCreatedAt()->format("d/m/Y");?></td>
                <td><?php if($bug->getClosedAt() != null){

                          echo $bug->getClosedAt()->format("d/m/Y");
                          }else{
                          echo "En Cours";
                          }
                    ?></td>
                <td><a href="bug/show/<?=$bug->getId();?>">Afficher</a></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
    <br><br>
  </div>

  <!--
  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Company Bio</h5>
          <p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>

        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>
  -->


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../../bin/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
