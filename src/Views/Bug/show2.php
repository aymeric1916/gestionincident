<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Starter Template - Materialize</title>

   <!-- Compiled and minified CSS -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
   <!-- Compiled and minified JavaScript -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
   <!-- CSS  -->
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <link href="/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
   <link href="style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Logo</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <div class="right">
        <img class="titreDroite" src="./images/utilisateurs.png" alt="Logo utilisateurs">
      </div>
      <h1 class="header">Rapport d'incident</h1>
      <br><br>

    </div>
  </div>


  <div class="container">
    <div class="section">
      <form class="col s12">
        <div class="row">
          <div class="input-field col s6">
            Nom de l'incident : Lorem ipsum dolor
          </div>
          <div class="input-field col s6">
            Date de l'observation : <input type="date" value="2020-09-28">
          </div>
          <br/>
        </div>
        <div class="row">
          <div class="input-field col s12">
            Description de l'incident : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed ligula lobortis, egestas ipsum quis, pulvinar erat. 
                                        Morbi quis nibh sit amet nibh vulputate bibendum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu lectus lacus. 
                                        Suspendisse cursus quam non magna lobortis, in blandit lacus ultrices. Sed porttitor, eros eu pulvinar finibus, augue neque efficitur justo, eu aliquet odio nisl a eros. 
                                        Nullam iaculis nisl auctor tortor bibendum sagittis. Quisque pulvinar interdum ullamcorper. Ut facilisis neque quis dolor elementum molestie. euismod tristique. 
          </div>
        </div>
          <br/>
      </form>

    </div>
    <br><br>
  </div>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../../bin/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
