<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $content = $this->render('src/Views/Bug/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        $bugs = [];

        // TODO: liste des incidents

        $content = $this->render('src/Views/Bug/list', ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        if(isset($_POST['submit']))
        {
                  $bug = new Bug();
                  $bug->setTitle($_POST['title']);
                  $bug->setDescription($_POST['description']);
                  $bug->setCreatedAt($_POST['createdAt'].'00:00:00');

                  $manager = new BugManager();
                  $manager->add($bug);
                  header('Location:'.PUBLIC_PATH.'bug');

        }
        else
        {
            $content = $this->render('src/Views/Bug/add', []);
            return $this->sendHttpResponse($content, 200);
        }
    }


}
